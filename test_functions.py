import pytest
from function import *
from article import *

def test_multiply():
   assert multiply(5, 4) == 20, "Résultat attendu : 20, Résultat obtenu : " + str(multiply(5, 4))
   assert multiply(-6, 4) == -24, "Résultat attendu : -24, Résultat obtenu : " + str(multiply(-6, 4))
   assert multiply(-9, -7) == 63, "Résultat attendu : 63, Résultat obtenu : " + str(multiply(-9, -7))
   assert multiply(0, 4) == 0, "Résultat attendu : 0, Résultat obtenu : " + str(multiply(0, 4))

def test_title_article():
   a = article("PopArTech", "News")
   assert title_article(a) == a.title, "Résultat attendu : PopArTech, Résultat obtenu : " + title_article(a)
   a = article("Python", "News")
   print(a.title)
   assert title_article(a) == a.title, "Résultat attendu : Python, Résultat obtenu : " + title_article(a)

def test_change_title_article():
   a = article("Vive iRoboTechArt", "News")
   change_name_article(a, "Vive PopArTech")
   assert a.title == "Vive PopArTech", "Résultat attendu : Vive PopArTech, Résultat obtenu : " + a.title

def test_add_article_to_list():
   a1 = article("1", "News")
   a2 = article("2", "Robotique")
   a3 = article("3", "News")
   articles = [a1, a2]
   add_article_to_list(a3, articles)
   assert articles[2] == a3, "Aucun ou le mauvais article a été ajouté"

def test_remove_article_to_list():
   a1 = article("1", "News")
   a2 = article("2", "Robotique")
   a3 = article("3", "News")
   articles = [a1, a2, a3]
   remove_article_to_list(articles)
   assert articles[0] == a1 and articles[1] == a2 and len(articles) == 2

def test_second_article():
   a1 = article("1", "News")
   a2 = article("2", "Robotique")
   a3 = article("3", "News")
   articles = [a1, a2, a3]
   assert second_article(articles) == a2, "Aucun ou mauvais article "

def test_articles_with_category():
   a1 = article("1", "News")
   a2 = article("2", "Robotique")
   a3 = article("3", "News")
   articles = [a1, a2, a3]
   category = "News"
   articles_with_good_category = articles_with_category(articles, category)
   for a in articles_with_good_category:
      assert(a.category == category)

def test_articles_with_two_categories():
   a1 = article("1", "News")
   a2 = article("2", "Robotique")
   a3 = article("3", "News")
   articles = [a1, a2, a3]
   category = "News"
   category2 = "Robotique"
   articles_with_good_category = articles_with_two_categories(articles, category, category2)
   for a in articles_with_good_category:
      assert(a.category == category or a.category == category2)

def test_display_articles_if_more_one_with_three_articles():
   a1 = article("1", "News")
   a2 = article("2", "Robotique")
   a3 = article("3", "News")
   articles = [a1, a2, a3]
   assert True == display_articles_if_more_one(articles)

def test_count_articles():
   a1 = article("1", "News")
   a2 = article("2", "Robotique")
   a3 = article("3", "News")
   articles = [a1, a2, a3]
   print(len(articles[:3]))
   assert len(articles) == count_articles(articles), "Résultat attendu : 3, Résultat obtenu : " + str(count_articles(articles))
   assert len(articles[:2]) == count_articles(articles[:2]), "Résultat attendu : 2, Résultat obtenu : " + str(count_articles(articles[:2]))